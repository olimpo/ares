<?php

namespace Ares\Bundle\CoreCustomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CoreCustomBundle:Default:index.html.twig', array('name' => $name));
    }
}
